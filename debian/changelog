dehydrated-hook-ddns-tsig (0.1.4-8) unstable; urgency=medium

  * d/watch: Switch to github/tags
  * d/changelog: Fix typo 'pyth' -> 'patch'
  * Bump standards version to 4.6.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 28 Sep 2022 10:10:44 +0200

dehydrated-hook-ddns-tsig (0.1.4-7) unstable; urgency=medium

  * Add patch to avoid deprecated dns.resolver.query()
    Thanks to Felix <felix@felix-zauberer.de> (Closes: #1005248)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 10 Feb 2022 09:50:33 +0100

dehydrated-hook-ddns-tsig (0.1.4-6) unstable; urgency=medium

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Modernized d/watch
  * Bump standards version to 4.6.0

  [ Debian Janitor ]
  * Set upstream metadata fields:
    Bug-Database, Bug-Submit, Repository, Repository-Browse.

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 24 Sep 2021 08:19:23 +0200

dehydrated-hook-ddns-tsig (0.1.4-5) unstable; urgency=medium

  * Patch to fix check whether adding/deleting a TXT-record succeeded
  * Note that patching for missing AAAA-records has been forwarded
  * Add salsa-ci configuration
  * Bump dh-compat to 13
  * Bump standards version to 4.5.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 27 Aug 2020 15:11:29 +0200

dehydrated-hook-ddns-tsig (0.1.4-4) unstable; urgency=medium

  * Switch to Python3 (Closes: #945635)
  * Switch dh-compat to 12
    * Replace d/compat with a versioned B-D on debhelper-compat
  * Bump standards version to 4.4.1
  * Drop obsolete d/source/local-options

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 10 Dec 2019 22:42:55 +0100

dehydrated-hook-ddns-tsig (0.1.4-3) unstable; urgency=medium

  * Backported fix for non-IPv6 nameservers
  * Refreshed patches with 'gbp pq'

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 25 Oct 2018 15:32:54 +0200

dehydrated-hook-ddns-tsig (0.1.4-2) unstable; urgency=medium

  * Patch to fix regression with key_algorithm missing in config-file.

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 24 Sep 2018 13:18:30 +0200

dehydrated-hook-ddns-tsig (0.1.4-1) unstable; urgency=medium

  * New upstream version 0.1.4
    * Closes: #908024
  * Stated that this package doesn't need 'root' to build
  * Bumped standards-version to 4.2.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Thu, 13 Sep 2018 13:15:49 +0200

dehydrated-hook-ddns-tsig (0.1.3-1) unstable; urgency=medium

  * New upstream version 0.1.3
    * Dropped patches applied upstream
  * Updated long description

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sun, 24 Jun 2018 19:30:21 +0200

dehydrated-hook-ddns-tsig (0.1.2-4) unstable; urgency=medium

  * Updated maintainer address to use the tracker.debian.org team.

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sat, 05 May 2018 21:48:00 +0200

dehydrated-hook-ddns-tsig (0.1.2-3) unstable; urgency=medium

  * Added patch to ignore unknown hooks.
    Thanks to Malcolm Scott (Closes: #895448)
  * Updated Vcs-* stanzas to salsa.d.o
  * Bumped standards version to 4.1.4

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sat, 14 Apr 2018 22:57:44 +0200

dehydrated-hook-ddns-tsig (0.1.2-2) unstable; urgency=medium

  * Backported patch to allow dashes at beginning of tokens
  * Bumped standards version to 4.1.3

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Mon, 08 Jan 2018 00:13:24 +0100

dehydrated-hook-ddns-tsig (0.1.2-1) unstable; urgency=medium

  * New upstream version 0.1.2
    (Closes: #873112)
  * Bumped standards to 4.1.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Sun, 01 Oct 2017 16:15:46 +0200

dehydrated-hook-ddns-tsig (0.1.1-1) unstable; urgency=medium

  * Initial release (Closes: #864408)

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 14 Jun 2017 20:39:35 +0200
